$(function() {

    // Memberships Page, Paypal Form / Inputs

    $("#paypal-membership-form-individual input").click(function() {
        var ppid = $(this).data('id');
        $("#ppchange-individual").val(ppid);
    });

    $("#paypal-membership-form-business input").click(function() {
        var ppid = $(this).data('id');
        $("#ppchange-business").val(ppid);
    });

    $("#paypal-membership-form-corporate input").click(function() {
        var ppid = $(this).data('id');
        $("#ppchange-corporate").val(ppid);
    });

    // Hamburger Nav Toggling
    $("#hamburger-icon").click(function() {
        console.log('clicked');
        $("#hamburger-nav").show();
    });

    $("#hamburger-x").click(function() {
        console.log('clicked');
        $("#hamburger-nav").hide();
    });

    // Academy Leaders Section Toggling
    $('.al-heading').click(function() {
        var sectionData = $(this).data('section');
        $('[data-id="' + sectionData + '"]').toggle();
        $(this).find('i').toggle();
    });

    // Memberships Page
    $('.individual-form').click(function() {
        var sectionData = $(this).data('section');
        if ($('fieldset[data-id="' + sectionData + '"]').height() === 380) {
            $('fieldset[data-id="' + sectionData + '"]').animate(
                {height: "74px"}, 200);
        }
        else if ($('fieldset[data-id="' + sectionData + '"]').height() === 74) {
            $('fieldset[data-id="' + sectionData + '"]').animate({height: "425px"},200);
        }
        $('form[data-id="' + sectionData + '"]').toggle();
        $('div[data-id="' + sectionData + '"]').toggle();

        $(this).find('i').toggle();
    });

    $('.business-form').click(function() {
        var sectionData = $(this).data('section');
        if ($('fieldset[data-id="' + sectionData + '"]').height() === 262) {
            $('fieldset[data-id="' + sectionData + '"]').animate(
                {height: "96px"}, 200);
        }
        else if ($('fieldset[data-id="' + sectionData + '"]').height() === 96) {
            $('fieldset[data-id="' + sectionData + '"]').animate({height: "262"},200);
        }
        $('form[data-id="' + sectionData + '"]').toggle();
        $('div[data-id="' + sectionData + '"]').toggle();

        $(this).find('i').toggle();
    });

    // Articles/Groups Ajax code for Admin Page
    function populateGroups(id) {

        $.ajax({
            url: '/getGroups',
            type: 'post',
            data: {id: id},
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    console.dir(response.groups);
                    //$('#group_select').val(response.name);

                    var groups = JSON.parse(response.groups);

                    var count = groups.length;

                    $('#groups_count').text(count);

                    $.each(groups, function(id, value) {
                        var name = value.name;
                        $('#group_select')
                            .append($('<option class="temp_group"></option>')
                                .attr("value", value.id)
                                .text(name));
                    });
                }
                else {
                    console.log('No Groups or Ajax call failed!');
                }
            }
        });
    }

    $("#category_select").change(function() {
        var id = $(this).val();
        $(".temp_group").remove();
        populateGroups(id);
    });

    if($("#category_select").length > 0) {
        var id = $("#category_select").val();
        populateGroups(id);
        console.log('onload');
    }

    // Active Nav Links
    var path = window.location.pathname.split( '/' );
    var current = "/" + path[1];
    $('a[href="' + current + '"]').addClass('active');

});