<html>
    <head>
        <title>@yield('title') | Oklahoma Academy</title>
        <meta charset="utf-8">
        <meta name="description" content="@yield('description')">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel='shortcut icon' href='/32x32.ico' type='image/x-icon' />

        {{--Styles--}}
        <link rel="stylesheet" href="/css/fonts/stylesheet.css" />
        <link rel="stylesheet" href="/css/layout.css" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

        {{--Scripts--}}
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
        <script src="/js/main.js"></script>

    </head>
    <body>
        @if(Auth::check())
            @include('admin/nav')
        @endif

        <div id="hamburger-nav">
            <div id="hamburger-x"><i class="fa fa-times"></i></div>
            <img src="/img/small-logo.png" />
            <ul class="link-list">
                <li><a href="/">HOME</a></li>
                <li><a href="/about">ABOUT</a></li>
                <li class="secondary"><a href="/about/annual-events">Annual Events</a></li>
                <li class="secondary"><a href="/about/academy-leaders">Academy Leaders</a></li>
                <li><a href="/news">NEWS</a></li>
                <li class="secondary"><a href="/news/blog">Blog</a></li>
                <li class="secondary"><a href="/news/town-hall-update">Town Hall Update</a></li>
                <li class="secondary"><a href="/news/calendar">Calendar</a></li>
                <li><a href="/memberships">MEMBERSHIPS</a></li>
                <li><a href="/library">LIBRARY</a></li>
                <li><a href="/contact">CONTACT</a></li>
            </ul>
            <div class="mobile-social">
                <a href="https://www.facebook.com/The-Oklahoma-Academy-76526497753/"><i class="fa fa-facebook"></i></a>
                <a href="https://twitter.com/okacademy?lang=en"><i class="fa fa-twitter"></i></a>
            </div>
        </div>

        <nav class="mobile-nav mobile-only">
            <div id="hamburger-icon">
                <i class="fa fa-bars"></i>
            </div>
            {{--<div class="small-logo">--}}
                {{--<a href="/"><img src="/img/small-logo.png" /></a>--}}
            {{--</div>--}}
            <div class="mobile-donate">
                <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JG3ZMKH8DAH5A"><p>DONATE</p></a>
            </div>
        </nav>

        <nav class="desktop-nav desktop-only link-list">
            <div class="nav-item nav-logo"><a href="/"><img class="nav-logo" src="/img/small-logo.png" /></a></div>
            <div class="nav-item"><a href="/">HOME</a></div>
            <div class="nav-item" id="about">
                <a href="/about">ABOUT</a>
                <div class="dropdown" id="about-dropdown">
                    <p><a href="/about/annual-events">ANNUAL EVENTS</a></p>
                    <p><a href="/about/academy-leaders">ACADEMY LEADERS</a></p>
                </div>
            </div>
            <div class="nav-item" id="news">
                <a href="/news">NEWS</a>
                <div class="dropdown" id="news-dropdown">
                    <p><a href="/news/blog">BLOG</a></p>
                    <p><a href="/news/town-hall-update">TOWN HALL UPDATE</a></p>
                    <p><a href="/news/calendar">CALENDAR</a></p>
                </div>
            </div>
            <div class="nav-item"><a href="/memberships">MEMBERSHIPS</a></div>
            <div class="nav-item"><a href="/library">LIBRARY</a></div>
            <div class="nav-item"><a href="/contact">CONTACT</a></div>
            <div class="header-social nav-item"><a href="https://www.facebook.com/The-Oklahoma-Academy-76526497753/">&nbsp;<i class="fa fa-facebook"></i></a></div>
            <div class="header-social nav-item"><a href="https://twitter.com/okacademy?lang=en">&nbsp;<i class="fa fa-twitter"></i></a></div>
            <div class="header-donate">
                <form id="donate" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="JG3ZMKH8DAH5A">
                    {{--<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donate_SM.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">--}}
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    <a onclick="document.getElementById('donate').submit();"><p>DONATE</p></a>
                </form>
            </div>
        </nav>

        <div class="page-content">
            @yield('content')
        </div>

        <footer>
            <div class="footer-wrapper">
                <div class="footer-left">
                    <p class="newsletter-signup">NEWSLETTER SIGN-UP</p>
                    <form id="newsletter-form" action="http://theoklahomaacademy.createsend.com/t/y/s/vtdtjk/" method="post" id="subForm">
                        <div class="form-input">
                            <input id="fieldName" name="cm-name" type="text" placeholder="YOUR NAME" />
                        </div>
                        <div class="form-input input-right">
                            <input id="fieldEmail" name="cm-vtdtjk-vtdtjk" type="email" placeholder="EMAIL" required />
                        </div>
                        <div class="form-input ns-submit">
                            <button type="submit">SUBMIT <i class="fa fa-angle-double-right"></i></button>
                        </div>
                    </form>
                </div>
                <div class="footer-right">
                    <div class="desktop-only">
                        <p class="address">405.307.0986 | 333 12th Street SE, #110, NORMAN, OK 73071</p>
                        <p class="fine-print">&copy;2016 The Oklahoma Academy.  All Rights Reserved. </p>
                    </div>
                    <div class="mobile-only">
                        <p class="address">405.307.0986</p>
                        <p class="address">333 12th Ave SE, #110, NORMAN, OK 73071</p>
                        <p class="fine-print">&copy;2016 The Oklahoma Academy.  All Rights Reserved. </p>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>
