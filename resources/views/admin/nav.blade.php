<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/13/16
 * Time: 10:33 PM
 */
?>
<link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<nav id="admin-nav">
    <div class="title">
        <a href="/admin">
            OK Academy Admin
        </a>
    </div>
    <div class="admin-link login">
        Logged In: {!! Auth::user()->name !!}
    </div>

    <div class="logout-link">
        <div style="padding-top:9px;"><a href="/logout">Logout</a></div>
    </div>
</nav>
