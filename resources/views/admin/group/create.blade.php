<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Groups | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <br>
                <h2 class="color-blue">Admin / Library / Groups / Create</h2>
                <br>

                {!!  Form::open(array('method' => 'post','route' => 'admin.group.store','files' => true))  !!}

                <div class="form-group">
                    <label for="title">Name</label>
                    {!! Form::text('name', NULL, ['class' => 'form-control', 'required' => 'required']) !!}
                </div>

                <div class="form-group">
                    <label for="category">Category</label>
                    {!! Form::select('category', $categories, NULL, array('id' => 'category_select', 'class' => 'form-control', 'required' => 'required')) !!}
                </div>

                <div class="form-group">
                    {!! Form::button('Add Group',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
