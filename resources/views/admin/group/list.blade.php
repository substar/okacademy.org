<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Posts - Groups | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">

                <?
                    if(session()->has('response')) echo session('response');

                    if(isset($filter)) echo $filter;
                ?>

                <br>
                <h2 class="color-blue">Admin / Library / Groups <small>{!! $groups->count() !!} groups</small></h2>
                <br>
                <a href="/admin/group/create" type="button" class="btn btn-success">Create Group</a>
                <br><br>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Manage</th>
                            <th>Category</th>
                            <th>Name</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                            foreach($groups as $group) {
                                echo '<tr>';
                                echo '<td><a href="/admin/group/' . $group->id . '/edit" class="btn btn-default btn-xs">Edit</a></td>';
                                echo '<td><a href="/admin/group?category_id=' . $group->category->id . '">' . $group->category->year_name . '</a></td>';
                                echo '<td><a href="/admin/library?group_id=' . $group->id . '">' . $group->name . '</a></td>';
                                echo '<td>' . $group->created_at . '</td>';
                                echo '<td>';
                                echo Form::open(array('route' => array('admin.group.destroy', $group->id), 'method' => 'delete'));
                                echo '<button type="submit" class="btn btn-danger btn-xs">Delete</button>';
                                echo Form::close();
                                echo '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
