<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */

?>
@extends('layouts.app')

@section('title', 'Admin - Features | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-centered panel panel-default">
                <? if(session()->has('response')) echo session('response'); ?>
                <br>
                <h2 class="color-blue">Admin / Features</h2>
                <br>

                <div class="row">
                    <div class="col-sm-4 col-sm-offset-1 panel panel-default">
                        <br>
                        <h4>Upcoming Event</h4>
                        <br>
                        {!!  Form::model($event,array('method' => 'put','route' => array('admin.feature.update', $event->id),'files' => true))  !!}

                            <div class="form-group">
                                <label for="title">Title</label>
                                {!! Form::text('title', NULL, ['class' => 'form-control', 'maxlength' => '47']) !!}
                            </div>

                            <div class="form-group">
                                <label for="title">Content (Max length 132 characters)</label>
                                {!! Form::textarea('content', NULL, ['class' => 'form-control', 'maxlength' => '132', 'rows' => '5']) !!}
                            </div>

                            <div class="form-group">
                                <label for="title">Link (use http:// for off-site links.)</label>
                                {!! Form::text('link', NULL, ['class' => 'form-control', 'maxlength' => '499']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::button('Update Event Feature',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                            </div>


                        {!! Form::close() !!}
                    </div>

                    <div class="col-sm-4 col-sm-offset-1 panel panel-default">
                        <br>
                        <h4>Featured Topic</h4>
                        <br>
                        {!!  Form::model($topic,array('method' => 'put','route' => array('admin.feature.update', $topic->id),'files' => true))  !!}

                            <div class="form-group">
                                <label for="title">Title</label>
                                {!! Form::text('title', NULL, ['class' => 'form-control', 'maxlength' => '47']) !!}
                            </div>

                            <div class="form-group">
                                <label for="title">Content (Max length 132 characters)</label>
                                {!! Form::textarea('content', NULL, ['class' => 'form-control', 'maxlength' => '132', 'rows' => '5']) !!}
                            </div>

                            <div class="form-group">
                                <label for="title">Link (use http:// for off-site links.)</label>
                                {!! Form::text('link', NULL, ['class' => 'form-control', 'maxlength' => '499']) !!}
                            </div>

                            <div class="form-group">
                                {!! Form::button('Update Topic Feature',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                            </div>


                        {!! Form::close() !!}
                    </div>
                </div>

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
