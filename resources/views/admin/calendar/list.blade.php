<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Calendar | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-centered panel panel-default">
                <br>
                <? if(session()->has('response')) echo session('response'); ?>

                <h2 class="color-blue">Admin / Library / Calendar / Edit Events</h2>
                <br>
                <div class="alert alert-warning"><strong>Note: </strong> Only edit one box at a time.</div>


                <div class="row">

                    @foreach($events as $event)

                        <div class="col-md-3">
                            {!!  Form::model($event,array('method' => 'put','route' => array('admin.calendar.update', $event->id)))  !!}

                            <div class="form-group">
                                <label for="show">Show</label>
                                {!! Form::checkbox('show', $event->show, $event->show) !!}
                                <br><br>
                                <label for="title">Name</label>
                                {!! Form::text('name', NULL, ['class' => 'form-control', 'maxlength' => '250']) !!}
                                <br>
                                <label for="title">Link (More Info Button)</label>
                                {!! Form::text('link', NULL, ['class' => 'form-control', 'maxlength' => '250']) !!}
                                <br>

                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker<?= $event->id?>'>
                                        <input name="start" value="<?= $event->start() ?>" type='text' class="form-control" />

                                        <span class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </span>
                                    </div>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $(function () {
                                            $('#datetimepicker<?=$event->id?>').datetimepicker();
                                        });
                                    });
                                </script>


                            </div>

                            <div class="form-group">
                                {!! Form::button('Update Event',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                            </div>

                            {!!  Form::close()  !!}
                        </div>
                    @endforeach
                </div>
                <br>
                <br>
            </div>
        </div>
    </div>

@stop
