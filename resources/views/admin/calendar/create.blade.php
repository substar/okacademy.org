<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Calendar | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <h2 class="color-blue">Admin / Library / Calendar / Create Event</h2>
                <br>

                {!!  Form::open(array('route' => 'admin.category.store'))  !!}

                <div class="form-group">
                    <label for="title">Name</label>
                    {!! Form::text('name', NULL, ['class' => 'form-control', 'maxlength' => '250']) !!}

                    <div class="container">
                        <div class="row">
                            <div class='col-sm-6'>
                                <div class="form-group">
                                    <div class='input-group date' id='datetimepicker1'>
                                        <input type='text' class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                                    </div>
                                </div>
                            </div>
                            <script type="text/javascript">
                                $(document).ready(function() {
                                    $(function () {
                                        $('#datetimepicker1').datetimepicker();
                                    });
                                });
                            </script>
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    {!! Form::button('Add Event',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
