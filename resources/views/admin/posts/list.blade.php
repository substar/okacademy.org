<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */

if($type == 'blog') {$header = 'Monthly Blog';}
if($type == 'town_hall') {$header = 'Town Hall';}

?>
@extends('layouts.app')

@section('title', 'Admin - Posts | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-centered panel panel-default">
                <? if(session()->has('response')) echo session('response'); ?>
                <br>
                <h2 class="color-blue">Admin / {!! $header !!} <small>{!! $posts->count() !!} posts</small></h2>
                <br>
                <a href="/admin/post/create?type={!! $type !!}" type="button" class="btn btn-success">Create {!! $header !!} Post</a>
                <br><br>
                @if($type == 'town_hall')
                    <div class="alert alert-warning"><strong>Note: </strong> The <a href="/town-hall-update">Town Hall Update</a> page will always show the latest of these posts.</div>
                @endif
                <table class="table">
                    <thead>
                        <tr>
                            <th>Manage</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Status</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                            foreach($posts as $post) {
                                echo '<tr>';
                                echo '<td><a href="/admin/post/' . $post->id . '/edit" class="btn btn-default btn-xs">Edit</a></td>';
                                echo '<td>' . $post->title . '</td>';
                                echo '<td>' . $post->author . '</td>';
                                echo '<td><a alt="View Post" class="btn btn-default btn-xs" href="/news/blog/' . $post->slug . '"><span class="glyphicon glyphicon-eye-open"></span></a>&nbsp; ' . $post->status() . '</td>';
                                echo '<td>' . $post->created_at . '</td>';
                                echo '<td>';
                                echo Form::open(array('route' => array('admin.post.destroy', $post->id), 'method' => 'delete'));
                                echo '<button type="submit" class="btn btn-danger btn-xs">Delete</button>';
                                echo Form::close();
                                echo '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
