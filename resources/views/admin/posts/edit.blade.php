<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
if($type == 'blog') {$header = 'Monthly Blog';}
if($type == 'town_hall') {$header = 'Town Hall';}

?>
@extends('layouts.app')

@section('title', 'Admin - Posts | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <br>
                <? if(isset($response)) echo $response; ?>
                <h2 class="color-blue">Admin / {!! $header !!} / Edit</h2>
                <br>

                {!!  Form::model($post,array('method' => 'put','route' => array('admin.post.update', $post->id),'files' => true))  !!}

                <input type="hidden" name="type" value="{!! $type !!}" />

                <div class="form-group">
                    <label for="title">Title</label>
                    {!! Form::text('title', NULL, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="text">Content</label>
                    {!! Form::textarea('content', NULL, ['class' => 'form-control widgEditor']) !!}
                </div>

                <div class="form-group">
                    <label for="pdf">Image (leave blank unless changing)</label>
                    <input type="file" name="img" />
                    @if(isset($post->img))
                        <p class="help-block">Current Image:</p>
                        <div style="max-width:500px; background-image:url('{!! $post->img !!}'); background-size: contain; background-repeat:no-repeat; position:relative;">
                            <a class="btn btn-danger" href="/admin/post/deleteImage?id={!! $post->id !!}" style="position: absolute; top:0; left:0;">DELETE</a>
                            <img src="{!! $post->img !!}" style="visibility: hidden; max-width: 500px;" />
                        </div>
                    @endif
                    <br>
                    <div class="radio">
                        <label>
                            <input type="radio" name="published" value="0" <? if($post->published == 0) echo 'checked'; ?>>
                            Draft
                        </label>
                    </div>
                    <div class="radio">
                        <label>
                            <input type="radio" name="published" value="1" <? if($post->published == 1) echo 'checked'; ?>>
                            Publish
                        </label>
                    </div>

                </div>

                <div class="form-group">
                    {!! Form::button('Update Post',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
