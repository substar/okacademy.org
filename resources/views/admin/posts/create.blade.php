<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
if($type == 'blog') {$header = 'Monthly Blog';}
if($type == 'town_hall') {$header = 'Town Hall';}

?>
@extends('layouts.app')

@section('title', 'Admin - Posts | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <br>
                <h2 class="color-blue">Admin / {!! $header !!} / Create</h2>
                <br>
                <div class="alert alert-warning">Keep Titles unique, i.e. 'February 2016', or 'My Cool Blog Post'.  Links are generated based on the title for SEO purposes.</div>

                {!!  Form::open(array('route' => 'admin.post.store', 'files' => true))  !!}

                <input type="hidden" name="type" value="{!! $type !!}" />

                <div class="form-group">
                    <label for="title">Title</label>
                    {!! Form::text('title', NULL, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="text">Content</label>
                    {!! Form::textarea('content', NULL, ['class' => 'form-control widgEditor']) !!}
                </div>

                <div class="form-group">
                    <label for="pdf">Image (optional)</label>
                    <input type="file" name="img" />
                </div>

                <div class="radio">
                    <label>
                        <input type="radio" name="published" value="0" checked>
                        Draft
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="published" value="1">
                        Publish
                    </label>
                </div>

                <div class="form-group">
                    {!! Form::button('Add Post',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
