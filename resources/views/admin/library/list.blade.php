<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Posts | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-10 col-centered panel panel-default">

                <?
                    if(session()->has('response')) echo session('response');

                    if(isset($filter)) echo $filter;
                ?>
                <br>
                <h2 class="color-blue">Admin / Library / Articles <small>{!! $articles->count() !!} articles</small></h2>
                <br>
                <a href="/admin/library/create" type="button" class="btn btn-success">Add Article</a>
                <br><br>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Manage</th>
                            <th>Title</th>
                            <th>Category</th>
                            <th>Group</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                            foreach($articles as $article) {
                                if(isset($article->groups_id))
                                    $group = '<a href="/admin/library?group_id=' . $article->group->id . '">' . $article->group->name . '</a>';
                                else
                                    $group = 'n/a';

                                echo '<tr>';
                                echo '<td><a href="/admin/library/' . $article->id . '/edit" class="btn btn-default btn-xs">Edit</a></td>';
                                echo '<td><a href="' . $article->pdf . '">' . $article->title . '</a></td>';
                                echo '<td><a href="/admin/library/?category_id=' . $article->category->id . '">' . $article->category->yearName() . '</a></td>';
                                echo '<td>' . $group . '</td>';
                                echo '<td>' . $article->created_at . '</td>';
                                echo '<td>';
                                echo Form::open(array('route' => array('admin.library.destroy', $article->id), 'method' => 'delete'));
                                echo '<button type="submit" class="btn btn-danger btn-xs">Delete</button>';
                                echo Form::close();
                                echo '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
