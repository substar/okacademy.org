<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Posts | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <br>
                <h2 class="color-blue">Admin / Library / Articles / Create</h2>
                <br>

                {!!  Form::open(array('method' => 'post','route' => 'admin.library.store','files' => true))  !!}

                <div class="form-group">
                    <label for="title">Title</label>
                    {!! Form::text('title', NULL, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="category">Category</label>
                    {!! Form::select('category', $categories, NULL, array('id' => 'category_select', 'class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    <label for="group">Group <small class="label label-default"><span id="groups_count">0</span> groups in this category.</small></label>
                    <select name="group" class="form-control" id="group_select">
                        <option value="" default>None</option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="pdf">PDF</label>
                    <input type="file" name="pdf" required />
                </div>

                <div class="form-group">
                    {!! Form::button('Add Article',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
