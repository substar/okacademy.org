<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Library | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <? if(session()->has('response')) echo session('response'); ?>
                <br>
                <h2 class="color-blue">Admin / Library / Articles / Edit</h2>
                <br>

                {!!  Form::model($article,array('method' => 'put','route' => array('admin.library.update', $article->id),'files' => true))  !!}

                <div class="form-group">
                    <label for="title">Title</label>
                    {!! Form::text('title', NULL, ['class' => 'form-control']) !!}
                </div>

                <div class="form-group">
                    <label for="category">Category</label>
                    {!! Form::select('category', $categories, $article->categories_id, array('id' => 'category_select', 'class' => 'form-control')) !!}
                </div>

                <div class="form-group">
                    <label for="pdf">PDF</label>
                    <input type="file" name="pdf" />
                </div>

                <div class="form-group">
                    {!! Form::button('Update Article',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop


