<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Categories | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <br>
                <h2 class="color-blue">Admin / Library / Categories / Create</h2>
                <br>

                {!!  Form::open(array('route' => 'admin.category.store'))  !!}

                <div class="form-inline">
                    <div class="form-group">
                        <label for="title">Year&nbsp;&nbsp;</label>
                        {!! Form::text('year', NULL, ['class' => 'form-control', 'maxlength' => '4', 'required' => 'required']) !!}
                    </div>

                    <div class="form-group">
                        <label for="title">&nbsp;&nbsp;Name&nbsp;&nbsp;</label>
                        {!! Form::text('name', NULL, ['class' => 'form-control', 'maxlength' => '250', 'required' => 'required']) !!}
                    </div>

                    <div class="form-group">
                        <label for="title">&nbsp;</label>
                        {!! Form::button('Add Category',array('type' => 'submit', 'class' => 'btn btn-success')) !!}
                    </div>
                </div>

                {!!  Form::close()  !!}

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
