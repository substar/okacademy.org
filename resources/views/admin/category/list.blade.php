<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/27/16
 * Time: 6:43 PM
 */
?>
@extends('layouts.app')

@section('title', 'Admin - Posts | Oklahoma Academy')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-centered panel panel-default">
                <? if(session()->has('response')) echo session('response'); ?>
                <br>
                <h2 class="color-blue">Admin / Library / Categories <small>{!! $categories->count() !!} categories</small></h2>
                <br>
                <a href="/admin/category/create" type="button" class="btn btn-success">Create Category</a>
                <br><br>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Manage</th>
                            <th>Year</th>
                            <th>Name</th>
                            <th>Groups</th>
                            <th>Date</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?
                            foreach($categories as $category) {
                                if(count($category->groups) > 0)
                                    $groups = '<a href="/admin/group?category_id=' . $category->id . '">' . count($category->groups) . '</a>';
                                else
                                    $groups = '';

                                echo '<tr>';
                                echo '<td><a href="/admin/category/' . $category->id . '/edit" class="btn btn-default btn-xs">Edit</a></td>';
                                echo '<td>' . $category->year . '</td>';
                                echo '<td>' . $category->name . '</td>';
                                echo '<td>' . $groups . '</td>';
                                echo '<td>' . $category->created_at . '</td>';
                                echo '<td>';
                                echo Form::open(array('route' => array('admin.category.destroy', $category->id), 'method' => 'delete'));
                                echo '<button type="submit" class="btn btn-danger btn-xs">Delete</button>';
                                echo Form::close();
                                echo '</td>';
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>

                <br>
                <br>
            </div>
        </div>
    </div>

@stop
