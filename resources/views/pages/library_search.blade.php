@extends('layout')

@section('title', 'Library - Search Results')

@section('description', 'Meta description here')

@section('content')

<div class="wrapper">
    <div class="top-spacer"></div>
    <h1 class="title-bar"><span>Library</span></h1>

    <div class="copy-box-md">
        <h3>{!! $articles->count() !!} Search result(s) for "{!! $terms !!}":</h3>
    </div>

    <br><br>

    <section class="library-group">

        @foreach($articles as $article)
            <a class="ib" href="{!! $article->pdf !!}">{!! $article->title !!} <i class="fa fa-angle-double-right"></i></a><br>
        @endforeach

    </section>


    <br><br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br><br>
</div>

@stop

