@extends('layout')

@section('title', 'Academy Leaders')

@section('description', 'Meta description here')

@section('content')
    <div class="wrapper sub-page">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>Academy Leaders</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around">
                BOARD OF DIRECTORS AND PARTNERS
            </div>
            <p></p>
        </div>

        <br><br>
        <br><br>
        <br><br>

        <div class="copy-box-md al-heading" data-section="executive-committee">
            <h3>EXECUTIVE COMMITTEE OF THE BOARD &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
        </div>

        <br>

        <section data-id="executive-committee">
            <div class="names">
                <div class="names-column">
                    <p class="title">Chairman of the Board</p>
                    <p class="name">Dan Boren</p>
                    <p class="name">President Corporate Development, the Chickasaw Nation, Oklahoma City</p>

                    <p class="title">Chairman-Elect</p>
                    <p class="name">Dave Stewart</p>
                    <p class="name">Executive Director &amp; Trustee, Mid-America Industrial Authority, Pryor</p>

                    <p class="title">Immediate Past Chairman; Strategic Thinking Co Chair</p>
                    <p class="name">John D. Harper</p>
                    <p class="name">VP External Affairs, PSO, Tulsa</p>

                    <p class="title">Strategic Thinking Co-Chair</p>
                    <p class="name">Sara Jane Smallwood</p>
                    <p class="name">Choctaw Nation, Durant</p>

                    <p class="title">Membership Vice President</p>
                    <p class="name">Michael Gordon</p>
                    <p class="name">Community Affairs Manager, PSO, Owasso</p>

                    <p class="title">Treasurer/Secretary</p>
                    <p class="name">Scott McLain</p>
                    <p class="name">H L Investments, Inc., OKC</p>

                    <p class="title">Vice Chair Treasurer</p>
                    <p class="name">John Budd</p>
                    <p class="name">Chief Development &amp; Strategy Officer, Sonic America’s Drive-In, OKC</p>

                    <p class="title">Investment Chair</p>
                    <p class="name">Doug Branch</p>
                    <p class="name">President, Biolytx, LLC., OKC</p>

                    <p class="title">Fund Development Chair</p>
                    <p class="name">Darryl Schmidt</p>
                    <p class="name">Executive VP, BancFirst, OKC</p>

                    <p class="title">Board Development Chair</p>
                    <p class="name">Susan Winchester</p>
                    <p class="name">The Winchester Group, Chickasha</p>
                </div>
                <div class="names-column">
                    <p class="name">John Budd<br>Chief Development &amp; Strategy Officer, Sonic Corp., Oklahoma City</p>
                    <p class="name">Mike Cooper<br>Cooper Strategies, LLC / AT&amp;T, Tulsa</p>
                    <p class="name">Lee Denney<br>Representative, Oklahoma House of Representatives, Cushing</p>
                    <p class="name">John Feaver, Ph.D.<br>President, University of Science &amp; Arts of Oklahoma, Chickasha</p>
                    <p class="name">Kay Goebel, Ph.D.<br>Psychologist, Oklahoma City</p>
                    <p class="name">Mickey Hepner, Ph.D.<br>Dean, University of Central Oklahoma College of Business, Edmond</p>
                    <p class="name">Cheryl Hill<br>President, Hill Manufacturing, Inc., Broken Arrow</p>
                    <p class="name">Rachel Hutchings<br>Director of Development, Tulsa Community College</p>
                    <p class="name">Lou Kohlman<br>Judicial Assistant, Court of Criminal Appeals, Oklahoma City</p>
                    <p class="name">Karen Langdon<br>Attorney, Legal Aid Services of Oklahoma, Tulsa</p>
                </div>
                <div class="names-column">
                    <p class="name">Anne Roberts<br>Director of Legislative Affairs, INTEGRIS Health, Oklahoma City</p>
                    <p class="name">Dave Rowland<br>President / CEO, Oklahoma Manufacturing Alliance, Tulsa</p>
                    <p class="name">Carolyn Stager<br>Executive Director, Oklahoma Municipal League, Oklahoma City</p>
                    <p class="name">David Stewart<br>Executive Director &amp; Trustee, Mid-America Trust Authority, Pryor</p>
                    <p class="name">Richard Wansley, Ph.D.<br>Senior Advisor Public Policy, Mental Health Association of Oklahoma, Tulsa</p>
                    <p class="name">Alba Weaver<br>Economic Development Project Manager, OG&amp;E, Oklahoma City</p>
                    <p class="name">Susan Winchester<br>President, The Winchester Group, Chickasha</p>
                </div>
            </div>
        </section>


        <div class="copy-box-md al-heading" data-section="board-members">
            <h3>BOARD MEMBERS &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
        </div>

        <br>

        <section data-id="board-members">
            <div class="names">
                <div class="names-column">
                    <p class="name">Phil Albert<br>Chairman &amp; CEO, Pelco Structural, LLC, Claremore</p>
                    <p class="name">Bev Binkowski<br>VP, Blue Cross Blue Shield, Oklahoma City</p>
                    <p class="name">Larry Briggs<br>President &amp; CEO, First National Bank and Trust Co., Shawnee</p>
                    <p class="name">Teresa Burkett<br>Attorney Conner &amp; Winters, Tulsa</p>
                    <p class="name">Stephanie Cameron<br> Air Power Systems Co., Inc., Tulsa</p>
                    <p class="name">Brett Campbell, Ph.D.<br>Provost &amp; Director of Workforce Development, Tulsa Community College</p>
                    <p class="name">Joe Dorman<br> Oklahoma Institute for Child Advocacy<br> Oklahoma City/Rush Springs</p>
                    <p class="name">Anita Poole Endsley<br>Consultant, Choctaw</p>
                    <p class="name">Amy Ford<br>Partner, RedAnt, LLC, Durant</p>
                    <p class="name">Brian Gaddy<br>Director Center for Business Dev, Autry Technology Center, Enid</p>
                    <p class="name">Molly Helm<br>Chief Thinker, Purple Ink, Enid</p>
                    <p class="name">Anne Holzberlein<br>VP Development &amp; ED UCO Foundation, Edmond</p>
                    <p class="name">Chuck Hoskin, Jr.<br>Secretary of State, Cherokee Nation, Tahlequah</p>
                </div>
                <div class="names-column">
                    <p class="name">Lundy Kiger<br>VP &amp; Director of Government Affairs, AES Shady Point, Panama</p>
                    <p class="name">Craig Knutson<br>Principal, Growing Global, LLC, Norman</p>
                    <p class="name">Barry Koonce<br> American Fidelity Assurance, Oklahoma City</p>
                    <p class="name">Terry Kordeliski<br>General Counsel, Auto Finance, Inc., Oklahoma City</p>
                    <p class="name">Thomas Lewis<br>Field Rep for Congressman Tom Cole, Lawton</p>
                    <p class="name">Josh McClintock<br>Partner, RedAnt, Edmond</p>
                    <p class="title">Treasurer and Secretary</p>
                    <p class="name">Scott McLain<br>Chief Investments Officer, H L Investments, LLC, Oklahoma City</p>
                    <p class="name">Darrell H. Mercer<br>Doctoral Candidate / Research Assistant, Oklahoma State University, Tulsa</p>
                    <p class="name">Chris Meyers<br>General Manager &amp; CEO, Oklahoma Association of Electric Cooperatives, Oklahoma City</p>
                    <p class="name">Mike Neal<br>President &amp; CEO, Tulsa Regional Chamber</p>
                    <p class="name">Susan Paddack<br>Senate, Oklahoma Senate, Ada</p>
                    <p class="name">Brenda Porton<br>Volunteer, Idabel</p>
                </div>
                <div class="names-column">
                    <p class="name">Lana Reyolds<br> Seminole State College, Seminole</p>
                    <p class="name">Larry Rice, Ph.D.<br>President, Rogers State University, Claremore</p>
                    <p class="name">Dan Schiedel<br>Executive Director & CEO<br> United Way of NW OK, Enid</p>
                    <p class="name">Sara Jane Smallwood<br>Public Information Officer, Choctaw Nation, Durant</p>
                    <p class="name">Clark Southard<br>Chairman, Oklahoma Southwest Alliance, Chickasha</p>
                    <p class="name">Craig Stephenson, City Manager, Ponca City</p>
                    <p class="name">Clayton Taylor<br>President, The Taylor Group, Oklahoma City</p>
                    <p class="name">Chuck Thompson<br>President and CEO, Republic Bank &amp; Trust, Norman</p>
                    <p class="name">Chris Tytanic<br>General Counsel, M3Technology Solutions, LLC, Norman</p>
                    <p class="name">Jeff Warmuth, President/CEO McAlester Chamber of Commerce, McAlester</p>
                    <p class="name">Sandy Washmon<br>Counselor, Woodward Public Schools</p>
                    <p class="name">Mathew Weaver<br>Manager &amp; Consultant, OKC Foreign Trade Zone, Oklahoma City</p>
                </div>
            </div>
        </section>

        <div class="copy-box-md al-heading" data-section="advisory-council">
            <h3>ADVISORY COUNCIL TO THE BOARD &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
        </div>

        <br>
        <section data-id="advisory-council">
            <div class="names">
                <div class="names-column">
                    <p class="name">Bill Anoatubby<br> Governor, the Chickasaw Nation</p>
                    <p class="name">Bill John Baker<br> Chief, Cherokee Nation</p>
                    <p class="name">Gary Batton, Chief<br> Choctaw Nation</p>
                    <p class="name">Chris Benge<br> Chief of Staff & Secretary of Native American Affairs</p>
                    <p class="name">Don Betz, Ph.D.<br> President, University of Central Oklahoma</p>
                    <p class="name">David Boren<br> President, University of Oklahoma</p>
                    <p class="name">Lauren Brookey<br> Vice President Tulsa Community College</p>
                    <p class="name">Robert Butkin<br>Professor of Law, The University of Tulsa</p>
                    <p class="name">Gerard Clancy, MD<br> TU President Designate</p>
                </div>
                <div class="names-column">
                    <p class="name">Steve Hahn<br> OK President, AT&amp;T</p>
                    <p class="name">Ted Haynes<br> President BCBS of Oklahoma</p>
                    <p class="name">Hans Helmerich<br> Chairman Helmerich &amp; Payne</p>
                    <p class="name">Senator David Holt<br>Oklahoma City</p>
                    <p class="name">Glen D. Johnson<br> Chancellor, State Regents for Higher Education</p>
                    <p class="name">Clark Jolley<br> Senator, Oklahoma Senate</p>
                    <p class="name">Bruce Lawrence<br> President &amp; CEO, INTEGRIS Health</p>
                    <p class="name">Marcie Mack<br> State Director, Career and Technology Education</p>
                    <p class="name">Scott Martin<br> Representative, Oklahoma House of Representatives</p>
                </div>
                <div class="names-column">
                    <p class="name">Tom McKeon, Ph.D.<br> Vice President &amp; Director, City Year Tulsa</p>
                    <p class="name">Paul Renfrow<br> VP Public Affairs, OG&amp;E</p>
                    <p class="name">Joe Siano<br> Superintendent, Norman Public Schools</p>
                    <p class="name">Mark Snead, RegionTrack, Inc., Yukon</p>
                    <p class="name">Stuart Solomon<br> President &amp; COO, Public Service Company</p>
                    <p class="name">Senator Stanislawski<br>Tulsa</p>
                    <p class="name">Steadman Upham, Ph.D.<br> President, The University of Tulsa</p>
                    <p class="name">Mike Turpen<br>Partner, Riggs Abney Neal Turpen et al, Oklahoma City</p>
                    <p class="name">Representative George Young<br> Oklahoma City</p>
                </div>
            </div>

            <br>

        </section>

        <div class="copy-box-md al-heading" data-section="regional-membership">
            <h3>2017 REGIONAL MEMBERSHIP CHAIRS & ADVISORS: &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
        </div>

        <br>
        <section data-id="regional-membership">
            <div class="names">
                <div class="names-column">
                    <p class="title">SW Region Chair</p>
                    <p class="name">Clark Southard<br> CEO/President<br>Chickasha</p>
                    <p class="title">SE Region Chair</p>
                    <p class="name">Jeff Warmuth<br> President &amp; CEO<br> McAlester Chamber of Commerce</p>
                </div>
                <div class="names-column">
                    <p class="title">NW Region Chair</p>
                    <p class="name">Sandy Washmon<br> Woodward Public Schools<br> Chair<br><br> Brian Gaddy<br> Vice Chair<br> Autry Tech, Enid<br><br> Immediate Past Chair<br> David Vanhooser<br> Enid</p>
                    <p class="title">NE Region Chair</p>
                    <p class="name">Craig Stephenson<br> City Manager<br> Ponca City<br><br> DJ Thomas<br> Muskogee Chamber Exec</p>
                </div>
                <div class="names-column">
                    <p class="title">Tulsa Metropolitan Area Region Chair</p>
                    <p class="name">Rachel Hutchings<br> TCC Foundation<br> On Metro Tulsa Committee<br>Teresa Burkett<br> Immediate Past Chair &amp; Advisor<br><br> Karen Langdon<br><br> Brett Campbell<br><br> Dave Rowland + others</p>
                    <p class="title">Oklahoma City Metropolitan Area Region Co-Chairs</p>
                    <p class="name">Carolyn Stager<br> Stick it in Stones, Inc.<br><br>Craig Knutson<br> Growing Global LLC, Norman<br><br> Co-Chairs<br> On Metro OKC Committee:<br> Alba Weaver<br> Immediate Past Chair &amp; Advisor<br><br> Dan Schiedel<br><br> Joe Siano<br> Norman<br><br> Josh McClintock<br> Edmond + others</p>
                </div>
            </div>

            <br>

        </section>

        <div class="copy-box-md al-heading" data-section="program">
            <h3>PROGRAM AND EVENT COMMITTEE CHAIRS &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
        </div>

        <br>
        <section data-id="program">
            <div class="names">
                <div class="names-column">
                    <p class="title">Legislators’ Welcome Reception Co-Chairs</p>
                    <p class="name">Clayton Taylor<br> The Taylor Group<br> OKC<br><br> Dan Schiedel<br> Schiedel Consulting, LLC.<br> Edmond<</p>
                    <p class="title">Salute Sales Committee Chair</p>
                    <p class="name">Dave Stewart<br> Mid America Industrial Authority<br> Pryor</p>
                </div>
                <div class="names-column">
                    <p class="title">2017 Town Hall Co-Chairs</p>
                    <p class="name">Dan Boren<br> Corporate Development w/ Chickasaw Nation<br><br> John Harper<br> Public Service Company of Oklahoma</p>
                    <p class="title">Research/Resource Committee Chair</p>
                    <p class="name">Craig Knutson<br> Potts Family Foundation &amp; Growing Global, LLC<br> Norman</p>
                </div>
                <div class="names-column">
                    <p class="title">Town Hall Training Chair</p>
                    <p class="name">Anita Poole Endsley<br> Choctaw</p>
                </div>
            </div>

            <br>

        </section>

        <div class="copy-box-md al-heading" data-section="member-partner">
            <h3>MEMBER PARTNER INVESTORS &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
        </div>

        <br>

        <section data-id="member-partner">
            <div class="names">
                <div class="names-column">
                    <p class="title" style="margin-bottom: 25px;">Catalyst Partner</p>
                    <img src="/img/cherokee.jpg" />

                </div>
            </div>
            <div class="names">
                <div class="names-column">
                    <p class="title" style="margin-bottom: 25px;">Consensus Partner</p>
                    <img src="/img/investors/oge.png" />

                </div>
            </div>

            <br>

            <div class="names">
                <div class="names-column">
                    <p class="title" style="margin-bottom: 25px;">Collaborator Partners</p>
                    <img src="/img/investors/aep.png" />
                    <img src="/img/investors/integris.png" />
                    <img src="/img/investors/att.png" />
                </div>
                <div class="names-column">
                    <p class="title" style="margin-bottom: 25px;"></p>
                    <img src="/img/investors/choctaw.png" />
                    <img src="/img/investors/bcbs.png" />
                </div>
                <div class="names-column">
                    <p class="title" style="margin-bottom: 25px;"></p>
                    <img src="/img/investors/bancfirst.png" />
                    <img src="/img/investors/chickasaw.png" />
                </div>
            </div>
        </section>
    </div>
    <br><br><br><br><br><br><br><br><br><br>
@stop