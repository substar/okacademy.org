@extends('layout')

@section('title', 'Town Hall Update')

@section('description', 'Meta description here')

@section('content')
    <div class="wrapper sub-page">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>Town Hall Update</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around">
                {!! $post->title !!}
            </div>
            <p>{!! $post->content !!}</p>
        </div>

        <br><br>
        <br><br>
        <br><br>
    </div>
@stop