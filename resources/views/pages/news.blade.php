<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/14/16
 * Time: 9:26 AM
 */
?>
@extends('layout')

@section('title', 'News')

@section('description', 'Meta description here')

@section('content')

    <div class="wrapper subpage">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>News</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around">
                KEEP IN THE KNOW
            </div>
            <p>State issues touch every single one of us, so it's important to stay informed about the latest challenges and proposed policies.  Find upcoming events, featured topics and updates from our Town Hall. We'll share any new developments, implementations and results here.</p>

            <br><br>

            <div class="news-container">
                <div class="blog-box">
                    <a href="/news/blog">
                        <fieldset>
                            <legend align="center"><img src="/img/yellow-flourish.png" /></legend>
                            <p class="name">BLOG</p>
                            <p class="cta">Featured topics <i class="fa fa-angle-double-right"></i></p>
                        </fieldset>
                    </a>
                </div>
                <div class="town-hall-box">
                    <a href="/news/town-hall-update">
                        <fieldset>
                            <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                            <p class="name">TOWN HALL</p>
                            <p class="cta">Annual event <i class="fa fa-angle-double-right"></i></p>
                        </fieldset>
                    </a>
                </div>
                <div class="calendar-box">
                    <a href="/news/calendar">
                        <fieldset>
                            <legend align="center"><img src="/img/yellow-flourish.png" /></legend>
                            <p class="name">Calendar</p>
                            <p class="cta">View events <i class="fa fa-angle-double-right"></i></p>
                        </fieldset>
                    </a>
                </div>
            </div>
            <br><br>
            <br><br>
            <br><br>
            <br><br>
        </div>
    </div>

@stop