@extends('layout')

@section('title', 'Library')

@section('description', 'Meta description here')

@section('content')

    <div class="wrapper subpage">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>Library</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around" style="height: 140px;">
                FIND YOUR<br>PUBLIC POLICY<br>PASSION
            </div>
            <p>Search for specific topics of interest or explore all of the issues.  Our library houses nearly every public policy issue in Oklahoma from the last three decades.  If you can't find what you're looking for here, more materials are available at our office.  <a href="/contact">Contact us.</a></p>
        </div>

        <div class="library-search">
            <form action="library-search" method="post">
                <input type="hidden" name="_token" value="{!! csrf_token() !!}" />
                <input class="search-box" type="text" name="search" />
                <input class="search-button" type="submit" value="SEARCH" />
            </form>
        </div>

        {{--<div class="copy-box-md al-heading" data-section="library-town-hall">--}}
            {{--<h3>OKLAHOMA ACADEMY TOWN HALLS &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>--}}
        {{--</div>--}}

        {{--<section data-id="library-town-hall">--}}
            {{--<div class="names">--}}
                {{--<div class="names-column">--}}
                    {{--<p class="library-link"><a href="#">The Town Hall Experience <i class="fa fa-angle-double-right"></i></a></p>--}}
                {{--</div>--}}
                {{--<div class="names-column">--}}
                {{--</div>--}}
                {{--<div class="names-column">--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</section>--}}
        @foreach($years as $year)
            <div class="copy-box-md">
                <h2 class="year">{!! $year !!}</h2>
            </div>
            @foreach($categories as $category)
                @if($category->year == $year)
                    <div class="copy-box-md al-heading" data-section="library-{!! $category->id !!}">
                        <h3>{!! strtoupper($category->name) !!} &nbsp; <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-up" style="display: none;"></i></h3>
                    </div>

                    <?
                        $count = $category->libraryCount();
                        $index = $category->libraryOrder();
                        $total = count($index);
                        $n = 0;
                    ?>

                    <section data-id="library-{!! $category->id !!}">
                        <div class="names">

                            @for($c = 0; $c < 3; $c++)
                                <div class="names-column">
                                    @for($i = 0; $i < $count; $i++)
                                        @if($n < $total)
                                            <p class="library-link"><a target="_blank" href="{!! $index[$n]['link'] !!}">{!! $index[$n]['name'] !!} <i class="fa fa-angle-double-right"></i></a></p>
                                        <?$n++;?>
                                        @endif
                                    @endfor
                                </div>
                            @endfor

                        </div>
                    </section>
                @endif
            @endforeach
        @endforeach

    </div>
@stop