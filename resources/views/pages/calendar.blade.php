<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/14/16
 * Time: 9:26 AM
 */

function venue($id) {
    $curl = curl_init();
    $hdr = array();
    $hdr[] = 'Authorization: Bearer MR6CXMLFDCMMVCCL22XN';
    $hdr[] = 'Content-type: application/json';

    curl_setopt($curl, CURLOPT_HTTPHEADER, $hdr);

    curl_setopt(
            $curl,
            CURLOPT_URL,
            'https://www.eventbriteapi.com/v3/venues/' . $id . '/'
    );

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    $venue = curl_exec($curl);
    $venue = get_object_vars(json_decode($venue));

    return $venue['name'];
}

use \Carbon\Carbon;

$curl = curl_init();
$hdr = array();
$hdr[] = 'Authorization: Bearer MR6CXMLFDCMMVCCL22XN';
$hdr[] = 'Content-type: application/json';

curl_setopt($curl, CURLOPT_HTTPHEADER, $hdr);

curl_setopt(
        $curl,
        CURLOPT_URL,
        'https://www.eventbriteapi.com/v3/users/me/owned_events/?status=all'
);

curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$events = curl_exec($curl);
$events = get_object_vars(json_decode($events));
$events = $events['events'];


if(isset($_GET['month']) && isset($_GET['year'])) {
        $get_month = $_GET['month'];
        $get_year  = $_GET['year'];

        $this_month = Carbon::parse($get_month . ' ' . $get_year);
        $month = $this_month->month;
        $year  = $this_month->year;
}
else {
        $this_month = new Carbon('this month');
        $month = $this_month->month;
        $year  = $this_month->year;
}

//dd($this_month);
$last_month = new Carbon($this_month);
$last_month->subMonth();
$next_month = new Carbon($this_month);
$next_month->addMonth();
$first_day = new Carbon('first day of ' . $this_month->format('F') . ' ' . $year);
$last_day  = new Carbon('last day of ' . $this_month->format('F') . ' ' . $year);


foreach($events as $event) {
}

?>
@extends('layout')

@section('title', 'Calendar')

@section('description', 'Meta description here')

@section('content')

        <div class="wrapper subpage">
                <div class="top-spacer"></div>
                <h1 class="title-bar"><span>Calendar</span></h1>

                <div class="copy-box-lg">
                        <div class="wrap-around contact">
                                PARTICIPATE<br>IN CHANGE
                        </div>
                        <p>The following are our biggest annual programs and events for raising awareness and engaging citizens across the state.</p>
                </div>

                <div class="clearfix"></div>

                <div class="four-box">
                    @foreach($fourevents as $fourevent)
                        <div class="box">
                            <div class="title">{!! $fourevent->name !!}</div>
                            <div class="date">{!! $fourevent->display() !!}</div>
                            <a href="{!! $fourevent->link !!}"><div class="more-info">MORE INFO <i class="fa fa-angle-double-right"></i></div></a>
                        </div>
                    @endforeach
                </div>

                <div class="copy-box-lg">
                    <p>Get hands-on with public policy development. Find an upcoming event that interests you and mark your calendar.</p>
                </div>

                <div class="current-month">{!! $this_month->format('F') !!}</div>

                <div class="date-nav">
                        <div class="last-month"><a href="/news/calendar?month={!! $last_month->format('F') !!}&year={!! $last_month->format('Y') !!}"><i class="fa fa-angle-double-left"></i>&nbsp;Previous Month</a></div>
                        <div class="this-month"> | </div>
                        <div class="next-month"><a href="/news/calendar?month={!! $next_month->format('F') !!}&year={!! $next_month->format('Y') !!}">Next Month&nbsp;<i class="fa fa-angle-double-right"></i></a></div>
                </div>

                <div class="events-container">
                    @foreach($events as $event)
                        <? $start_time = Carbon::parse($event->start->local); ?>
                        <? $end_time = Carbon::parse($event->end->local); ?>

                        @if(($start_time >= $first_day) && ($start_time <= $last_day))
                                <div class="event">
                                <a href="{!! $event->url !!}">
                                    <fieldset>
                                        <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                                        <p class="day">{!! $start_time->format('d') !!}</p>
                                        <p class="title">{!! $event->name->text !!}</p>
                                        <p class="time">{!! $start_time->format('g:iA') !!}-{!! $end_time->format('g:iA') !!}</p>
                                        @if(isset($event->venue_id))
                                            <p class="venue">&#64; {!! venue($event->venue_id)  !!}</p>
                                        @endif
                                        <div class="more-info">MORE INFO <i class="fa fa-angle-double-right"></i></div>
                                    </fieldset>
                                </a>
                            </div>
                            <? $event_exists = 1; ?>
                        @endif
                    @endforeach
                    @if(!isset($event_exists))
                        <p class="no-events">No events scheduled for this month.</p>
                    @endif
                </div>

        </div>
@stop