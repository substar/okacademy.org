@extends('layout')

@section('title', 'About')

@section('description', 'Meta description here')

@section('content')
    <div class="wrapper sub-page">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>About</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around">
                MOVING IDEAS INTO ACTION
            </div>
            <p>The Oklahoma Academy is a nonprofit organization dedicated to improving our state, one public policy at a time. But, we don’t work alone.  We believe the people should have a say in their futures and quality of life, so we engage citizens across Oklahoma and involve them in every step of the proposal process. Our mission is to:</p>
        </div>

        <div class="copy-box-md">
            <h3>RAISE AWARENESS OF THE ISSUES</h3>
            We host <a href="/news/calendar">annual events and programs</a>, where we help citizens understand how public policy issues affect how we live, work and play.
        </div>

        <div class="copy-box-md">
            <h3>PROVIDE NON-PARTISAN, EVIDENCE-BASED RESEARCH</h3>
            Nearly every public policy issue in Oklahoma from the last three decades can be found in our extensive library.
        </div>

        <div class="copy-box-md">
            <h3>HOLD SPACE FOR CIVIL DISCOURSE AND CONSENSUS BUILDING</h3>
            At our Town Hall event, we give citizens the opportunity to honestly and openly discuss the issues, determine the solutions, and collaborate to develop public policies that will achieve the greatest good. Then, we empower them to lobby their legislators and other policy makers.
            <br><br>
            We’ve covered a wide range of topics, including education, small business development, government structure, crime, technology and the future, and the state's constitution. We’ve achieved many milestones and accomplishments, and thanks to citizens like you, we continue to grow in numbers, reach and influence.
        </div>

        <div class="fs-box-section">
            <div class="row">
                <fieldset class="w-one mobile-w">
                    <legend align="center">525</legend>
                    <strong>members</strong> of the<br> <strong>Oklahoma<br> Academy</strong>
                </fieldset>
                <fieldset class="w-two mobile-w">
                    <legend align="center">64</legend>
                    <strong>pieces of legislation passed</strong> since the<br>adoption of the <strong>Town Hall process</strong> in 2001<br><br>
                </fieldset>
            </div>
            <div class="row">
                <fieldset class="w-three mobile-w">
                    <legend align="center">7,665</legend>
                    <strong>participants</strong> in the 33 conferences<br>held <strong>since 1987</strong>
                </fieldset>
                <fieldset class="w-four mobile-w">
                    <legend align="center">160</legend>
                    <strong>scholarships provided</strong> to enable college students to attend Town Hall events<strong>&nbsp;</strong>
                </fieldset>
            </div>
        </div>

        <div class="flourish-section">
            <div class="about-memberships">
                <a href="/memberships" target="_blank">
                    <fieldset>
                        <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                        <p class="name">MEMBERSHIPS</p>
                        <p class="cta">Get involved <i class="fa fa-angle-double-right"></i></p>
                    </fieldset>
                </a>
            </div>
            <div class="about-donate">
                <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JG3ZMKH8DAH5A" target="_blank">
                    <fieldset>
                        <legend align="center"><img src="/img/yellow-flourish.png" /></legend>
                        <p class="name">DONATE</p>
                        <p class="cta">Support the Academy <i class="fa fa-angle-double-right"></i></p>
                    </fieldset>
                </a>
            </div>
        </div>

        <h1 class="title-bar"><span>Staff</span></h1>

        <section class="staff">
            <div class="member">
                <img src="/img/member-julie-knutson.jpg" />
                <p class="name">Julie Knutson</p>
                <p class="title">President and CEO</p>
            </div>
            <div class="member">
                <img src="/img/member-lori-harless.jpg" />
                <p class="name">Lori Harless</p>
                <p class="title">Research / Resources Coordinator / Salute Coordinator</p>
            </div>
            <div class="member">
                <img src="/img/member-jamie-wade.jpg" />
                <p class="name">Jamie Wade</p>
                <p class="title">Executive Assistant to the CEO / Membership & Town Hall Manager</p>
            </div>
        </section>
        <h1 class="title-bar"><span>History</span></h1>

        <section class="history">
            <div class="copy-box-lg">
                <div class="wrap-around">
                    PUBLIC POLICY RULED BY THE PEOPLE
                </div>
                <p>Former Oklahoma governor and U.S. Senator Henry Bellmon knew the people were an important part of the equation for a successful state. After his first term as governor, Bellmon founded the Oklahoma Academy for State Goals in 1967 to create a public policy organization that was independent, nonpartisan and inclusive. His vision, which remains today, was to empower Oklahomans to improve their quality of life through effective public policy development and implementation.</p>
            </div>

            <div class="copy-box-md">
                The Oklahoma Academy was revitalized in 1985 and has been led by current President and CEO Julie Knutson since 1987. From 1985 to 2000, the Oklahoma Academy used a traditional conference format to form recommendations addressing critical public policy issues facing the state. Since 2001, the Oklahoma Academy has used a successful Town Hall model.
            </div>
        </section>
        <h1 class="title-bar"><span>Bellmon Legacy Fund Campaign</span></h1>

        <section class="bellmon">
            <div class="copy-box-lg">
                <div class="wrap-around">
                    HELP FUND OUR MISSION
                </div>
                <p>Named in honor of the Oklahoma Academy’s founder, the Bellmon Legacy Fund Campaign seeks to support our operations by raising a $3 million endowment and $500,000 Reserve Savings Fund. When fully funded, this endowment will generate about $150,000 in annual earnings, which will be used for:</p>
            </div>

            <div class="copy-box-md">
                <h3>UNIVERSITY STUDENT SCHOLARSHIPS</h3>
                <p>Currently, 10 college students are provided scholarships to participate in the annual Town Hall, but the process would benefit from the involvement of high school students and Oklahoma professionals, who may need financial assistance to attend.</p>
            </div>

            <div class="copy-box-md">
                <h3>OUTREACH ACTIVITIES</h3>
                <p>Once Town Hall recommendations are officially released, we would hold follow-up forums and meetings in key areas of the state. This would very likely lead to more partnerships in putting the recommendations into action. </p>
            </div>

            <div class="copy-box-md">
                <p>Help us continue our vital work for the people and the future of Oklahoma. Make a donation today. You may also download, print, complete and return this donation form to our office, or contact Jamie Wade for further assistance. </p>
            </div>
            <br><br><br>
        </section>
    </div>
@stop