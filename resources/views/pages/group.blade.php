@extends('layout')

@section('title', 'Library - ' . $group->category->name . ' - ' . $group->name)

@section('description', 'Meta description here')

@section('content')

<div class="wrapper">
    <div class="top-spacer"></div>
    <h1 class="title-bar"><span>Library</span></h1>

    <div class="copy-box-md">
        <h3>{!! $group->category->yearName() !!}</h3>
        <p>{!! $group->name !!}</p>
    </div>

    <section class="library-group">

        @foreach($articles as $article)
            <a class="ib" href="{!! $article->pdf !!}">{!! $article->title !!} <i class="fa fa-angle-double-right"></i></a><br>
        @endforeach

    </section>
    <br><br><br><br><br><br><br>
    <br><br><br><br><br><br><br>



</div>

@stop

