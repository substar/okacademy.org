<?php
/**
 * Created by PhpStorm.
 * User: ryanrobinson
 * Date: 2/14/16
 * Time: 9:26 AM
 */
?>
@extends('layout')

@section('title', 'Memberships')

@section('description', 'Meta description here')

@section('content')

    <div class="wrapper subpage">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>Memberships</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around">
                BE A PART OF OKLAHOMA'S PROGRESS
            </div>
            <p>Your action today impacts your tomorrow. Join us and fellow citizens as we pave a better way for Oklahoma, now and into the future, through effective public policies.</p>
        </div>

        <h1 class="title-bar"><span>Annual Membership Rates</span></h1>

        <div class="memberships-container">
            <div class="memberships-box">
                <fieldset data-id="individual-form">
                    <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                    <p class="title individual-form" data-section="individual-form">Individual <i class="fa fa-angle-double-down"></i><i class="fa fa-angle-double-right" style="display: none;"></i></p>

                    <form id="paypal-membership-form-individual" data-id="individual-form" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" id="ppchange-individual" name="hosted_button_id" value="FYF74FGFPTWMU">
                        <ul>
                            <li>
                                <input type="radio" id="a-option" name="selector" checked data-id="FYF74FGFPTWMU">
                                <label for="a-option">Full-time Student: $25</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="aa-option" name="selector" data-id="ULBVF2BVTWA6L">
                                <label for="aa-option">Young Professional: $50</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="b-option" name="selector" data-id="P5CC7ZNYM6L8W">
                                <label for="b-option">Spouse of Member: $75</label>

                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="c-option" name="selector" data-id="TGNGZNER85UY2">
                                <label for="c-option">Regular: $150</label>

                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="d-option" name="selector" data-id="MCVTKW6427P38">
                                <label for="d-option">Leader: $250</label>

                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="e-option" name="selector" data-id="2KCPH6J53QZHL">
                                <label for="e-option">Advocate: $500</label>

                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="f-option" name="selector" data-id="54ZASWZ4XVQKL">
                                <label for="f-option">Champion: $1,000</label>

                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="g-option" name="selector" data-id="XDNBE82DF67ZS">
                                <label for="g-option">Lifetime: $3,000</label>

                                <div class="check"></div>
                            </li>
                        </ul>
                        {{--<input type="image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFFg4HMLcPbYzfxBpIDFCVTGC0I_xDHVaRrJPrhU_SWRw9cixf" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">--}}
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>
                    <div class="pay-online" data-id="individual-form" onclick="document.getElementById('paypal-membership-form-individual').submit();">PAY ONLINE <i class="fa fa-angle-double-right"></i></div>
                </fieldset>
            </div>
            <div class="memberships-box">
                <fieldset class="business" data-id="business-form">
                    <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                    <p class="title business-form" data-section="business-form">Businesses/<br> Institutions <i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-down" style="display: none;"></i></p>

                    <form id="paypal-membership-form-business" data-id="business-form" class="business" data-id="business" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" id="ppchange-business" name="hosted_button_id" value="CSTM7X8QGNN82">
                        <ul>
                            <li>
                                <input type="radio" id="a2-option" name="selector" checked data-id="CSTM7X8QGNN82">
                                <label for="a2-option">Activist: $500</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="b2-option" name="selector" data-id="2HA2GTZLRUDJ8">
                                <label for="b2-option">Innovator: $1,000</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="c2-option" name="selector" data-id="QHY758QXDDDKS">
                                <label for="c2-option">Ambassador: $3,000</label>
                                <div class="check"></div>
                            </li>
                        </ul>
                        {{--<input type="image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFFg4HMLcPbYzfxBpIDFCVTGC0I_xDHVaRrJPrhU_SWRw9cixf" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">--}}
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>
                    <div class="pay-online" style="display:none;" data-id="business-form" onclick="document.getElementById('paypal-membership-form-business').submit();">PAY ONLINE <i class="fa fa-angle-double-right"></i></div>
                </fieldset>
            </div>
            <div class="memberships-box">
                <fieldset class="business corporate" data-id="corporate-form">
                    <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                    <p class="title business-form" data-section="corporate-form">Corporate<br> Investors <i class="fa fa-angle-double-right"></i><i class="fa fa-angle-double-down" style="display: none;"></i></p>

                    <form id="paypal-membership-form-corporate" data-id="corporate-form" class="corporate" action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" id="ppchange-corporate" name="hosted_button_id" value="NDVL66JD5Q7X6">
                        <ul>
                            <li>
                                <input type="radio" id="a3-option" name="selector" checked data-id="NDVL66JD5Q7X6">
                                <label for="a3-option">Silver: $5,000</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="b3-option" name="selector" data-id="NB3HGCXQ4PCY6">
                                <label for="b3-option">Gold: $10,000</label>
                                <div class="check"></div>
                            </li>
                            <li>
                                <input type="radio" id="c3-option" name="selector" data-id="7KBZWBX6KXTKG">
                                <label for="c3-option">Platinum: $15,000</label>
                                <div class="check"></div>
                            </li>
                        </ul>
                        {{--<input type="image" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSFFg4HMLcPbYzfxBpIDFCVTGC0I_xDHVaRrJPrhU_SWRw9cixf" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">--}}
                        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>
                    <div class="pay-online" style="display: none;" data-id="corporate-form" onclick="document.getElementById('paypal-membership-form-corporate').submit();">PAY ONLINE <i class="fa fa-angle-double-right"></i></div>
                </fieldset>
            </div>
        </div>

        <div class="memberships-bottom">
            <p>Not ready to become a member but want to contribute to our mission? <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=JG3ZMKH8DAH5A">Donate today.</a></p>
        </div>

    </div>

@stop