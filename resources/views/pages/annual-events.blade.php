@extends('layout')

@section('title', 'About')

@section('description', 'Meta description here')

@section('content')
    <div class="wrapper sub-page">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>Annual Events</span></h1>

        <div class="copy-box-lg">
            <div class="wrap-around">
                <div class="large">DISCUSS THE ISSUES.</div>
                <div class="medium">DETERMINE THE SOLUTIONS.</div>
                <div class="medium">DEVELOP THE POLICIES</div>
            </div>
            <h3>TOWN HALL CONFERENCE RETREAT</h3>
            <p>The Town Hall event is the heart of what we do and a unique experience for Oklahomans. Held annually in late October, the Town Hall renews the arts of listening, discussing and determining a direction on a given issue. Invitations are sent out across the state, and then 150 participants are selected from the responses. The selected participants are an accurate representation of the socio-demographic composition of Oklahoma, lending the opportunity for everyone in the state to be heard. The group spends three days answering questions on the researched issue under focus and coming to a consensus on the solution. Recommendations from the Town Hall are then implemented through community and legislative action. Visit our library to read about past topics and results.</p>
        </div>

        <br>

        <style>.embed-container { position: relative; padding-bottom: 56.25%; height: 0; overflow: hidden; max-width: 100%; } .embed-container iframe, .embed-container object, .embed-container embed { position: absolute; top: 0; left: 0; width: 100%; height: 100%; }</style><div class='embed-container'><iframe src='https://www.youtube.com/embed/m7flWClJ0KI' frameborder='0' allowfullscreen></iframe></div>

        <br>

        <div class="copy-box-md">
            <h3>ANNUAL ACADEMY SALUTE</h3>
            <p>Spend a fun-filled evening at our annual fundraiser, the Salute. During this event, we honor Oklahoma's congressional delegation, statewide elected and appointed officials, and the legislative leadership. The Oklahoma Key Contributor Award is our most prestigious acknowledgement and thank you for participation, involvement and support by an individual, business, organization or program that has resulted in significant positive change for Oklahoma and in the overall quality of life for all Oklahomans.</p>
        </div>

        <div class="copy-box-md">
            <h3>LEGISLATORS WELCOME RECEPTION</h3>
            <p>Welcome back legislators to the spring session. Every February or March, we host a reception where you can meet and speak with elected officials. A brief program portion of the reception features a succinct review of the most recent Town Hall consensus recommendations and a brief highlight of topical legislation.  </p>
        </div>

        <div class="copy-box-md">
            <h3>REGIONAL FORUMS</h3>
            <p>Come meet fellow interesting and interested Oklahomans. Every spring, the Oklahoma Academy hosts regional forums on the Town Hall solutions from the most recent Town Hall, along with topical issues. We share ideas, facts and our perspectives, and then encourage discussion and debate among the attendees. The forums are designed to bring citizens together, raise awareness of the issues and encourage citizens to communicate with their elected officials.</p>
            <p>During gubernatorial election years, we engage the public to find out more about the candidates and issues at a special forum. All of the candidates for governor are invited and receive an outline of possible questions that cover some Town Hall topics. At the end of the forum, audience members may ask questions, and candidates will be given time for a final statement or rebuttal.</p>
            <p><a class="ib" href="/memberships">Join the Oklahoma Academy</a> now to never miss our updates and events.</p>
        </div>

        <br><br><br><br><br><br>
    </div>
@stop