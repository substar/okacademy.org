@extends('layout')

@section('title', $post->title)

@section('description', 'Meta description here')

@section('content')
    <div class="wrapper sub-page">
        <div class="top-spacer"></div>
        <h1 class="title-bar"><span>Blog</span></h1>

        <div class="copy-box-lg">
            @if(isset($post->img))
                <p><img src="{!! $post->img !!}" style="max-width: 100%;" /></p>
            @endif
            <div class="wrap-around">
                {!! $post->title !!}
            </div>
            {!! $post->content !!}
            @if(isset($post->img))
                <p><img src="{!! $post->img !!}" style="max-width: 100%;" /></p>
            @endif
        </div>
        <div class="blog-details">{!! $post->author !!} | {!! $post->created_at->toFormattedDateString() !!}</div>
        <div class="blog-details-two">
            <div class="inline"><a class="read" href="/news/blog/{!! $post->slug !!}">READ ARTICLE <i class="fa fa-angle-double-right"></i></a></div>
            <div class="inline"><a href="https://www.facebook.com/sharer/sharer.php?u=http%3A//okacademy.org/{!! Request::path() !!}"><div class="blog-social"><span><i class="fa fa-facebook"></i></span></div></a></div>
            <div class="inline"><a href="https://twitter.com/home?status={!! $post->twitter() !!}%20%40%20http%3A//okacademy.org/{!! Request::path() !!}"><div class="blog-social"><span><i class="fa fa-twitter"></i></span></div></a></div>
            <div class="inline share">SHARE</div>
        </div>

        <br><br>
        <br><br>
        <br><br>
    </div>
@stop