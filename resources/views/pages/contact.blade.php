@extends('layout')

@section('title', 'Contact')

@section('description', 'Meta description here')

@section('content')

<div class="wrapper">
    <div class="top-spacer"></div>
    <h1 class="title-bar"><span>Contact</span></h1>

    <div class="copy-box-lg">
        <div class="wrap-around contact">
            LET YOUR VOICE BE HEARD
        </div>
        <p>For more information about us, specific public policy topics, or to get involved with the Oklahoma Academy right away, call 405.307.0986 or email President and CEO Julie Knutson at <a class="ib" href="mailto:julie@okacademy.org">julie@okacademy.org.</a></p>
    </div>

    <div class="contact-methods">
        <div class="method">
            <p class="type">CALL</p>
            <p><a href="tel:4053070986">405.307.0986</a></p>
        </div>
        <div class="method">
            <p class="type">EMAIL</p>
            <p><a href="mailto:julie@okacademy.org">julie@okacademy.org</a></p>
        </div>
        <div class="method">
            <p class="type">ADDRESS</p>
            <p>401 W. Main St., #390</p>
            <p>Norman, OK 73069</p>
        </div>
    </div>
    <br><br><br>
</div>

@stop

