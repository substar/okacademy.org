@extends('layout')

@section('title', 'Home')

@section('description', 'Meta description here')

@section('content')
    <div class="home-header">
        <img src="/img/home-header.jpg" class="desktop-only" />
        <img src="/img/mobile-header.jpg" class="mobile-only" />
        <div class="home-caption">
                <fieldset>
                    <legend align="center">TOGETHER,</legend>
                    <p class="f1">WE SHAPE</p>
                    <p class="f2">OKLAHOMA'S</p>
                    <p class="f3">PUBLIC POLICY</p>
                </fieldset>
        </div>
    </div>
    <div class="home-bottom-bar">
        <div class="first">
            <div class="home-memberships">
                <a href="/memberships">
                    <fieldset>
                        <legend align="center"><img src="/img/blue-flourish.png" /></legend>
                        <p class="name">MEMBERSHIPS</p>
                        <p class="cta">Get involved <i class="fa fa-angle-double-right"></i></p>
                    </fieldset>
                </a>
            </div>
            <div class="home-donate">
                <a onclick="document.getElementById('donate').submit();">
                    <fieldset>
                        <legend align="center"><img src="/img/yellow-flourish.png" /></legend>
                        <p class="name">DONATE</p>
                        <p class="cta">Support the Academy <i class="fa fa-angle-double-right"></i></p>
                    </fieldset>
                </a>
            </div>
        </div>
        <a href="{!! $event->link !!}" class="feature">
            <div class="home-news second">
                <div class="left">
                    <img src="/img/calendar-icon.png" />
                </div>
                <div class="right">
                    <p class="title">UPCOMING EVENT</p>
                    <p class="info">{!! $event->title !!}</p>
                    <p class="blurb desktop-only">{!! $event->content !!}  <i class="fa fa-angle-double-right"></i></p>
                </div>
                <div class="blurb mobile-only">{!! $event->content !!}  <i class="fa fa-angle-double-right"></i></div>
            </div>
        </a>
        <a href="{!! $topic->link !!}" class="feature">
            <div class="home-news third">
                <div class="left">
                    <img src="/img/topic-icon.png" />
                </div>
                <div class="right">
                    <p class="title">FEATURED TOPIC</p>
                    <p class="info">{!! $topic->title !!}</p>
                    <p class="blurb desktop-only">{!! $topic->content !!}  <i class="fa fa-angle-double-right"></i></p>
                </div>
                <div class="blurb mobile-only">{!! $topic->content !!}  <i class="fa fa-angle-double-right"></i></div>
            </div>
        </a>
    </div>
@stop