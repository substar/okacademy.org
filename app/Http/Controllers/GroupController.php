<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article\Article;
use App\Article\Category;
use App\Article\Group;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect, Input, Response, View;


class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //

        $data = $request->all();

        //Check for filters
        if(isset($data['category_id'])) {
            $category = Category::find($data['category_id']);
            $filter = '<br><div class="alert alert-warning" role="alert">Filtered by <strong>Category:</strong> ' . $category->name . '.  <a href="/admin/group">See all groups.</a></div>';
            $groups = Group::where('categories_id', '=', $category->id)->get();
            return View::make('admin.group.list')->with('groups', $groups)->with('filter', $filter);
        }
        else {
            $groups = Group::all();
            return View::make('admin.group.list')->with('groups', $groups);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $categories = Category::lists('year_name','id');
        return View::make('admin.group.create')->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = $request->all();

        $group = new Group;

        $group->name = $data['name'];
        $group->categories_id = $data['category'];

        $response = '<br><div class="alert alert-success" role="alert">Group added successfully.</div>';

        $group->save();

        return Redirect::to('admin/group')->with('response',$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $group = Group::find($id);

        $categories = Category::lists('year_name','id');
        return View::make('admin.group.edit')->with('categories', $categories)->with('group', $group);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $group = Group::find($id);

        $group->name = $data['name'];
        $group->categories_id = $data['category'];

        $response = '<br><div class="alert alert-success" role="alert">Group updated.</div>';

        $group->save();

        return Redirect::to('admin/group')->with('response',$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // Check for Articles belonging to this group
        $articles = Article::where('groups_id', '=', $id)->get();

        if(!$articles->isEmpty()) {
            $response = '<br><div class="alert alert-danger" role="alert"><strong>Error: </strong>Can\'t delete group while it still contains Articles. View <a href="/admin/library?group_id=' . $id . '">here.</a></div>';
            return Redirect::back()->with('response', $response);
        }

        try {
            $group = Group::find($id);

            $group->delete();
        }
        catch (\Exception $e) {
            $response = '<br><div class="alert alert-danger" role="alert">Something went wrong.</div>';
            return Redirect::back()->with('response', $response);
        }
        $response = '<br><div class="alert alert-danger" role="alert">Group deleted.</div>';
        return Redirect::back()->with('response', $response);
    }
}
