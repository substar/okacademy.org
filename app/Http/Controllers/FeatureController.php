<?php

namespace App\Http\Controllers;

use App\Feature\Feature;
use Illuminate\Http\Request;

use App\Http\Requests;

use View, Input, Redirect;

class FeatureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $event = Feature::find(1);
        $topic = Feature::find(2);

        return View::make('admin.feature.list')->with('topic', $topic)->with('event', $event);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $data = $request->all();

        $feature = Feature::find($id);

        $feature->title = $data['title'];
        $feature->content = nl2br($data['content']);
        $feature->link = $data['link'];


        $feature->save();

        $response = '<br><div class="alert alert-success" role="alert">Feature updated.</div>';

        return Redirect::to('admin/feature')->with('response',$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
