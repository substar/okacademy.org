<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Events\Event;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use View, DateTime, Redirect;

class CalendarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $events = Event::all();
        return View::make('admin.calendar.list')->with('events', $events);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('admin.calendar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $event = Event::find($id);

        if(isset($data['show']))
            $event->show = 1;
        else
            $event->show = 0;

        $event->name = $data['name'];
        $event->link = $data['link'];

        $start = new DateTime($data['start']);

        $event->start_date = $start->format('Y-m-d H:i:s');

        $event->save();

        $response = '<br><div class="alert alert-success" role="alert">Event updated.</div>';

        return Redirect::to('admin/calendar')->with('response',$response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
