<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth, View;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Post\Post;

class AdminPageController extends Controller
{
    //

    public function index() {
        return View::make('admin.admin');
    }
}
