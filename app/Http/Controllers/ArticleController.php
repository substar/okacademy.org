<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Article\Article;
use App\Article\Category;
use App\Article\Group;
use App\Post\Post;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Redirect, Input, Response, View, Exception;

class ArticleController extends Controller
{
    //
    public function index(Request $request)
    {
        //
        $data = $request->all();

        //Check for filters
        if(isset($data['group_id'])) {
            $group = Group::find($data['group_id']);
            $filter = '<br><div class="alert alert-warning" role="alert">Filtered by <strong>Group:</strong> ' . $group->name . ', <strong>Category:</strong> ' . $group->category->name . '.  <a href="/admin/library">See all articles.</a></div>';
            $articles = Article::where('groups_id', '=', $group->id)->get();
            return View::make('admin.library.list')->with('articles', $articles)->with('filter', $filter);
        }
        elseif(isset($data['category_id'])) {
            $category = Category::find($data['category_id']);
            $filter = '<br><div class="alert alert-warning" role="alert">List filtered by <strong>Category: </strong>' . $category->name . '. <a href="/admin/library">See all articles.</a></div>';
            $articles = Article::where('categories_id', '=', $data['category_id'])->get();
            return View::make('admin.library.list')->with('articles', $articles)->with('filter', $filter);
        }
        else {
            $articles = Article::all();
            return View::make('admin.library.list')->with('articles', $articles);
        }
    }

    public function create()
    {
        //
        $categories = Category::lists('year_name','id');
        return View::make('admin.library.create')->with('categories', $categories);
    }

    public function store(Request $request)
    {
        //
        $data = $request->all();

        $article = new Article;

        $article->title = $data['title'];
        $article->categories_id = $data['category'];

        if($data['group'] != "")
            $article->groups_id = $data['group'];
        else
            $article->groups_id = NULL;

        $article->save();

        $file = $data['pdf'];

        if ($file instanceof UploadedFile) {
            $destinationPath = public_path() . '/uploads/articles';
            $filename = $file->getClientOriginalName();
            $article->pdf = '/uploads/articles/' . $article->id . '-' . $filename;
            $extension = $file->getClientOriginalExtension();
            $file->move($destinationPath, $article->pdf, $extension);
        }

        $response = '<br><div class="alert alert-success" role="alert">Article added successfully.</div>';

        $article->save();

        return Redirect::to('admin/library')->with('response',$response);
    }

    public function edit($id)
    {
        $article = Article::find($id);

        $categories = Category::lists('name','id');
        return View::make('admin.library.edit')->with('categories', $categories)->with('article', $article);
    }

    public function update(Request $request, $id)
    {
        $data = $request->all();

        try {

            $article = Article::find($id);

            $article->title = $data['title'];
            $article->categories_id = $data['category'];

            if(isset($data['group']))
                $article->groups_id = $data['group'];
            else
                $article->groups_id = NULL;

            if(isset($data['pdf'])) {

                $file = $data['pdf'];

                if ($file instanceof UploadedFile) {
                    $destinationPath = public_path() . '/uploads/articles';
                    $filename = $file->getClientOriginalName();
                    $article->pdf = '/uploads/articles/' . $article->id . '-' . $filename;
                    $extension = $file->getClientOriginalExtension();
                    $file->move($destinationPath, $article->pdf, $extension);
                }
            }

            $article->save();
        }
        catch (\Exception $e) {
            $response = '<br><div class="alert alert-danger" role="alert">Something went wrong.</div>';
            return Redirect::back()->with('response', $response);
        }

        $response = '<br><div class="alert alert-success" role="alert">Article updated.</div>';
        return Redirect::to('admin/library')->with('response',$response);
    }

    public function destroy($id)
    {
        //
        try {
            $article = Article::find($id);
            $article->delete();
        }
        catch (\Exception $e) {
            $response = '<br><div class="alert alert-danger" role="alert">Something went wrong.</div>';
            return Redirect::back()->with('response', $response);
        }
        $response = '<br><div class="alert alert-danger" role="alert">Article deleted.</div>';
        return Redirect::back()->with('response', $response);
    }

    //Ajax for Create/Edit Article: Populate Groups form select on Category change
    public function getGroups(Request $request)
    {
        $id = $request->input('id');

        $category = Category::find($id);
        $groups = Group::where('categories_id', '=', $category->id)->get();

        $json = $groups->toJson();

        $response = ['success' => true, 'groups' => $json];
        return Response::json($response);
    }
}
