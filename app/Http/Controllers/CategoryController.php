<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Article\Article;
use App\Article\Category;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Redirect, Input, Response, View;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = Category::all();
        return View::make('admin.category.list')->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return View::make('admin.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $data = $request->all();

        $category = new Category;

        $category->name = $data['name'];
        $category->year = $data['year'];
        $category->year_name = $data['year'] . ' - ' . $data['name'];

        $response = '<br><div class="alert alert-success" role="alert">Category added successfully.</div>';

        $category->save();

        return Redirect::to('admin/category')->with('response',$response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $category = Category::find($id);

        return View::make('admin.category.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $category = Category::find($id);

        $category->name = $data['name'];
        $category->year = $data['year'];
        $category->year_name = $data['year'] . ' - ' . $data['name'];

        $response = '<br><div class="alert alert-success" role="alert">Category updated.</div>';

        $category->save();

        return Redirect::to('admin/category')->with('response',$response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try {
            $category = Category::find($id);
            $category->delete();
        }
        catch (\Exception $e) {
            $response = '<br><div class="alert alert-danger" role="alert">Something went wrong.</div>';
            return Redirect::back()->with('response', $response);
        }
        $response = '<br><div class="alert alert-danger" role="alert">Category deleted.</div>';
        return Redirect::back()->with('response', $response);
    }
}
