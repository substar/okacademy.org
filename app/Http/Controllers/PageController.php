<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use View, Auth, Redirect;
use App\Article\Article;
use App\Article\Group;
use App\Article\Category;
use App\Events\Event;
use App\Feature\Feature;
use App\Post\Post;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

class PageController extends Controller
{
    //
    public function about() {
        return View::make('pages.about');
    }

    public function academy_leaders() {
        return View::make('pages.academy-leaders');
    }

    public function annual_events() {
        return View::make('pages.annual-events');
    }

    public function blog() {
        $posts = Post::where('type', '=', 'blog')->where('published', '=', 1)->orderBy('created_at', 'desc')->paginate(2);

        return View::make('pages.blog')->with('posts', $posts);
    }

    public function blog_post($slug)
    {
        $post = Post::where('slug', '=',$slug)->first();

        if($post->published == 0){
            if(Auth::check())
                return View::make('pages.blog-post')->with('post', $post);
            else
                return Redirect::to('/news/blog');
        }

        return View::make('pages.blog-post')->with('post', $post);
    }

    public function calendar() {
        $fourevents = Event::where('show','=', '1')->get();

        return View::make('pages.calendar')->with('fourevents', $fourevents);
    }

    public function contact() {
        return View::make('pages.contact');
    }

    public function group() {
        $input = Input::all();

        $articles = Article::where('groups_id', '=', $input['id'])->get();
        $group = Group::find($input['id']);

        return View::make('pages.group')->with('articles', $articles)->with('group', $group);
    }

    public function home() {
        $event = Feature::find(1);
        $topic = Feature::find(2);

        return View::make('pages.home')->with('topic', $topic)->with('event', $event);;
    }

    public function library() {

        $categories = Category::all();
        $cat_years = Category::orderBy('year','desc')->groupBy('year')->get();

        $years = array();
        foreach($cat_years as $cat_year)  $years[] = $cat_year->year;

        return View::make('pages.library')->with('categories', $categories)->with('years', $years);
    }

    public function news() {
        return View::make('pages.news');
    }

    public function memberships() {
        return View::make('pages.memberships');
    }

    public function library_search(Request $request) {

        $data = $request->all();

        $articles = Article::where('title', 'LIKE', '%' . $data['search'] . '%')->get();

        return View::make('pages.library_search')->with('articles', $articles)->with('terms', $data['search']);
    }

    public function town_hall() {
        $post = Post::orderBy('created_at', 'desc')->first();

        return View::make('pages.town-hall-update')->with('post', $post);
    }

}
