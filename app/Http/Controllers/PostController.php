<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post\Post;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use View, Redirect, Response, Auth, Image;

class PostController extends Controller
{
    //
    public function index()
    {
        $input = Input::all();

        $type = $input['type'];

        if($type == 'blog') {$posts = Post::where('type', '=', 'blog')->orderBy('created_at', 'desc')->get();}
        if($type == 'town_hall') {$posts = Post::where('type', '=', 'town_hall')->get();}

        return View::make('admin.posts.list')->with('type', $type)->with('posts', $posts);
    }

    public function delete_image(Request $request)
    {
        $data = $request->all();

        $post = Post::find($data['id']);
        $post->img = NULL;
        $post->save();

        $response = '<br><div class="alert alert-success" role="alert">Image removed.</div>';

        return Redirect::to('admin/post/' . $post->id .'/edit')->with('type', $post->type)->with('response', $response);
    }

    public function create()
    {
        $input = Input::all();

        $type = $input['type'];

        return View::make('admin.posts.create')->with('type', $type);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $post = new Post;

        $post->type = $data['type'];
        $post->title = $data['title'];

        $post->slug = strtolower(preg_replace('/\s+/', '-', $data['title']));

        $post->content = nl2br($data['content']);
        $post->author = Auth::user()->name;

        $post->save();

        if(isset($data['img'])) {
            $img = $data['img'];

            if ($img instanceof UploadedFile) {

                $image = Image::make($img);

                $extension = $img->getClientOriginalExtension();
                $path = '/uploads/posts/';
                $filename = $post->id . '.' . $extension;
                $image->save(public_path() . $path . $filename, 100);

                $post->img = $path . $filename;
            }
        }

        $response = '<br><div class="alert alert-success" role="alert">Post added successfully.</div>';

        $post->save();

        return Redirect::to('admin/post?type=' . $data['type'])->with('response',$response)->with('type', $data['type']);
    }

    public function edit($id)
    {
        //
        $post = Post::find($id);

        return View::make('admin.posts.edit')->with('type', $post->type)->with('post', $post);
    }

    public function update(Request $request, $id)
    {
        //
        $data = $request->all();

        $post = Post::find($id);

        $post->title = $data['title'];
        $post->content = nl2br($data['content']);

        $post->published = $data['published'];

        $post->slug = strtolower(preg_replace('/\s+/', '-', $data['title']));

        $post->save();

        if(isset($data['img'])) {
            $img = $data['img'];

            if ($img instanceof UploadedFile) {

                $image = Image::make($img);

                $extension = $img->getClientOriginalExtension();
                $path = '/uploads/posts/';
                $filename = $post->id . '.' . $extension;
                $image->save(public_path() . $path . $filename, 100);

                $post->img = $path . $filename;
            }
        }

        $response = '<br><div class="alert alert-success" role="alert">Post updated.</div>';

        $post->save();

        return Redirect::to('admin/post?type=' . $data['type'])->with('response',$response);
    }

    public function destroy($id)
    {
        //
        try {
            $post = Post::find($id);
            $post->delete();
        }
        catch (\Exception $e) {
            $response = '<br><div class="alert alert-danger" role="alert">Something went wrong.</div>';
            return Redirect::back()->with('response', $response);
        }
        $response = '<br><div class="alert alert-danger" role="alert">Post deleted.</div>';
        return Redirect::back()->with('response', $response);
    }
}
