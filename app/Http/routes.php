<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::post('getGroups', 'ArticleController@getGroups');


Route::group(['middleware' => 'web'], function () {
    Route::auth();

    Route::get('/', 'PageController@home');

    // Subpages
    Route::get('/about', 'PageController@about');
    Route::get('/about/academy-leaders', 'PageController@academy_leaders');
    Route::get('/about/annual-events', 'PageController@annual_events');
    Route::get('/contact', 'PageController@contact');
    Route::get('/library', 'PageController@library');
    Route::get('/library/group', 'PageController@group');
    Route::post('/library-search', 'PageController@library_search');
    Route::get('/memberships', 'PageController@memberships');
    Route::get('/news', 'PageController@news');
    Route::get('/news/blog', 'PageController@blog');
    Route::get('/news/blog/{slug}', 'PageController@blog_post');
    Route::get('/news/calendar', 'PageController@calendar');
    Route::get('/news/town-hall-update', 'PageController@town_hall');

    // Auth
    Route::get('login', 'Auth\AuthController@getLogin');
    Route::post('login', 'Auth\AuthController@postLogin');
    Route::get('auth/logout', 'Auth\AuthController@getLogout');

    //Ajax

});

// Admin Pages
Route::group(['middleware' => ['web', 'auth']], function () {
    Route::get('/admin/post/deleteImage', 'PostController@delete_image');

    Route::get('/home', 'HomeController@index');
    Route::get('/admin', 'AdminPageController@index');

    // Resources
    Route::resource('/admin/category', 'CategoryController');
    Route::resource('/admin/group', 'GroupController');
    Route::resource('/admin/library', 'ArticleController');
    Route::resource('/admin/post', 'PostController');
    Route::resource('/admin/feature', 'FeatureController');
    Route::resource('/admin/calendar', 'CalendarController');

});