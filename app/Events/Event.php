<?php

namespace App\Events;

use Illuminate\Database\Eloquent\Model;

use DateTime;

class Event extends Model
{
    //
    protected $table = 'events';

    public function start() {
        $start = new DateTime($this->start_date);
        return $start->format('m/d/Y H:i:s');
    }

    public function display() {
        $start = new DateTime($this->start_date);
        return $start->format('m/d/Y');
    }
}