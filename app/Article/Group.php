<?php

namespace App\Article;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    //
    protected $table = 'groups';

    public function category() {
        return $this->belongsTo('App\Article\Category', 'categories_id');
    }

    public function link()
    {
        return '/library/group?id=' . $this->id;
    }
}
