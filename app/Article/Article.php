<?php

namespace App\Article;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';

    public function category() {
        return $this->belongsTo('App\Article\Category', 'categories_id');
    }

    public function group() {
        return $this->belongsTo('App\Article\Group', 'groups_id');
    }
}
