<?php

namespace App\Article;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //

    protected $table = 'categories';

    public function libraryCount()
    {
        $groups = Group::where('categories_id', '=', $this->id)->get();

        $articles = Article::where('categories_id', '=', $this->id)->where('groups_id', '=', NULL)->get();

        $group_count = $groups->count();
        $article_count = $articles->count();

        $total = $group_count + $article_count;

        if($total <= 3)
            $column_count = 1;
        elseif($total <= 5)
            $column_count = 2;
        else
            $column_count = (int) ($total / 3);

        return $column_count;
    }

    public function libraryOrder()
    {
        $groups = Group::where('categories_id', '=', $this->id)->get();
        $group_count = $groups->count();

        $articles = Article::where('categories_id', '=', $this->id)->where('groups_id', '=', NULL)->get();

        $article_layout = array();

        $i = 0;
        foreach($articles as $article) {
            $article_layout[$i]['link'] = $article->pdf;
            $article_layout[$i]['name'] = $article->title;
            $i++;
        }

        foreach($groups as $group) {
            $article_layout[$i]['link'] = $group->link();
            $article_layout[$i]['name'] = $group->name;
            $i++;
        }

        return $article_layout;
    }

    public function yearName()
    {
        return $this->year . ' - ' . $this->name;
    }

    public function groups()
    {
        return $this->hasMany('App\Article\Group', 'categories_id');
    }

    public function articles()
    {
        return $this->hasMany('App\Article\Article', 'categories_id');
    }


}
