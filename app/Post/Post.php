<?php

namespace App\Post;

use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

class Post extends Model
{
    //
    protected $table = 'posts';

    public function twitter()
    {
        return str_replace(' ', '%20', $this->title);
    }

    public function preview() {
        return mb_strimwidth($this->content, 0, 999, "...");
    }

    public function status() {
        if($this->published == 1) return '<span style="color: green">Published</span>';
        if($this->published == 0) return '<span style="color: grey">Draft</span>';
    }

//    public function date()
//    {
//        $date = Carbon::createFromDate($this->created_at);
//
//        echo $date->toFormattedDateString();
//    }
}
